//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#![deny(
    clippy::unwrap_used,
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces
)]

mod commands;
mod inputs;

use crate::commands::Command;
use crate::inputs::PkOrScript;
use commands::{
    balance::balance, current_ud::current_ud, idty::idty, members_count::members_count,
    wallet::wallet,
};
#[cfg(test)]
use dubp_client::MockGvaClient;
use dubp_client::{
    crypto::keys::{ed25519::PublicKey, PublicKey as _},
    wallet::prelude::*,
    AccountBalance, Amount, GvaClient, Idty, NaiveGvaClient, PaymentResult, PubkeyOrScript,
};
use read_input::prelude::*;
use std::{
    env::var_os,
    fs::File,
    io::{BufReader, Read, Write},
    path::PathBuf,
    str::FromStr,
    time::Instant,
};
use structopt::StructOpt;

const DEFAULT_GVA_SERVER: &str = "https://g1.librelois.fr/gva";

#[derive(StructOpt)]
#[structopt(name = "rust-gva-client", about = "Client use GVA API of Duniter.")]
struct CliArgs {
    /// GVA server url
    #[structopt(short, long, default_value = DEFAULT_GVA_SERVER)]
    server: String,
    #[structopt(subcommand)]
    command: Command,
}

fn main() -> anyhow::Result<()> {
    let cli_args = CliArgs::from_args();

    let gva_client = NaiveGvaClient::new(&cli_args.server)?;
    let mut out = std::io::stdout();

    match cli_args.command {
        Command::Balance {
            pubkey_or_script,
            ud_unit,
        } => balance(&gva_client, &mut out, &pubkey_or_script, ud_unit)?,
        Command::CurrentUd => current_ud(&gva_client, &mut out)?,
        Command::Idty { pubkey } => idty(&gva_client, &mut out, &pubkey)?,
        Command::MembersCount => members_count(&gva_client, &mut out)?,
        Command::Wallet { command } => wallet(&gva_client, &mut out, command)?,
    }
    Ok(())
}
