//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;
use dubp_client::{
    crypto::keys::PublicKey as _,
    documents_parser::{wallet_script_from_str, TextParseError},
};

pub struct PkOrScript(pub(crate) WalletScriptV10);

impl FromStr for PkOrScript {
    type Err = TextParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Ok(pubkey) = PublicKey::from_base58(s) {
            Ok(Self(WalletScriptV10::single_sig(pubkey)))
        } else {
            Ok(Self(wallet_script_from_str(s)?))
        }
    }
}
