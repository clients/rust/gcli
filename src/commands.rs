//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

pub mod balance;
pub mod current_ud;
pub mod idty;
pub mod members_count;
pub mod wallet;

use crate::*;

#[allow(clippy::large_enum_variant)]
#[derive(StructOpt)]
pub(crate) enum Command {
    /// Get account balance
    Balance {
        pubkey_or_script: String,
        #[structopt(short, long)]
        ud_unit: bool,
    },
    /// Get current UD value
    CurrentUd,
    /// Get identity
    Idty { pubkey: String },
    /// Get current number of WoT members
    MembersCount,
    /// Create or update a wallet
    Wallet {
        #[structopt(subcommand)]
        command: wallet::WalletCommand,
    },
}
