//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;

pub(crate) fn idty<C: GvaClient, W: Write>(
    gva_client: &C,
    out: &mut W,
    pubkey: &str,
) -> anyhow::Result<()> {
    let pubkey = PublicKey::from_base58(pubkey)?;

    let req_time = Instant::now();
    if let Some(Idty {
        is_member,
        username,
        ..
    }) = gva_client.idty_by_pubkey(pubkey)?
    {
        println!(
            "The server responded in {} ms.",
            req_time.elapsed().as_millis()
        );

        writeln!(out, "Found identity for pubkey:")?;
        writeln!(out, "username: {}", username)?;
        writeln!(out, "is_member: {}", is_member)?;
    } else {
        writeln!(out, "No identity for pubkey {}", pubkey)?;
    }

    Ok(())
}
