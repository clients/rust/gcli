//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;

pub(crate) fn current_ud<C: GvaClient, W: Write>(
    gva_client: &C,
    out: &mut W,
) -> anyhow::Result<()> {
    let req_time = Instant::now();
    if let Some(current_ud) = gva_client.current_ud()? {
        println!(
            "The server responded in {} ms.",
            req_time.elapsed().as_millis()
        );

        let int_part = current_ud / 100;
        let dec_part = current_ud % 100;
        writeln!(
            out,
            "The current UD value is {}.{:02} Ğ1 !",
            int_part, dec_part
        )?;
    } else {
        writeln!(out, "server with empty blockchain")?;
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_current_ud() -> anyhow::Result<()> {
        let mut client = MockGvaClient::default();
        client.expect_current_ud().returning(|| Ok(Some(1_023)));
        let mut out = Vec::new();
        current_ud(&client, &mut out)?;
        let output = std::str::from_utf8(&out)?;

        assert_eq!(output, "The current UD value is 10.23 Ğ1 !\n");

        Ok(())
    }
}
