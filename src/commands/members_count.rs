//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;

pub(crate) fn members_count<C: GvaClient, W: Write>(
    gva_client: &C,
    out: &mut W,
) -> anyhow::Result<()> {
    let req_time = Instant::now();
    let members_count = gva_client.members_count()?;
    println!(
        "The server responded in {} ms.",
        req_time.elapsed().as_millis()
    );

    writeln!(
        out,
        "There is currently {} members in Ğ1 WoT!",
        members_count
    )?;

    Ok(())
}

// below are tests

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_member_count() -> anyhow::Result<()> {
        let mut client = MockGvaClient::default();
        client.expect_members_count().returning(|| Ok(10_000));
        let mut out = Vec::new();
        members_count(&client, &mut out)?;
        let output = std::str::from_utf8(&out)?;

        assert_eq!(output, "There is currently 10000 members in Ğ1 WoT!\n");

        Ok(())
    }
}
