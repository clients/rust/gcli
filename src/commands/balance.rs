//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;

pub(crate) fn balance<C: GvaClient, W: Write>(
    gva_client: &C,
    out: &mut W,
    pubkey_or_script: &str,
    ud_unit: bool,
) -> anyhow::Result<()> {
    let pubkey_or_script = PubkeyOrScript::from_str(pubkey_or_script)?;

    let req_time = Instant::now();
    if let Some(AccountBalance {
        amount,
        ud_amount_opt,
    }) = gva_client.account_balance(&pubkey_or_script, ud_unit)?
    {
        println!(
            "The server responded in {} ms.",
            req_time.elapsed().as_millis()
        );

        if let Some(ud_amount) = ud_amount_opt {
            writeln!(
                out,
                "The balance of account '{}' is {:.2} UDĞ1 !",
                pubkey_or_script.to_string(),
                amount.amount() as f64 / ud_amount.amount() as f64,
            )?;
        } else {
            writeln!(
                out,
                "The balance of account '{}' is {}.{:02} Ğ1 !",
                pubkey_or_script.to_string(),
                amount.amount() / 100,
                amount.amount() % 100
            )?;
        }
    } else {
        writeln!(
            out,
            "Account '{}' not exist !",
            pubkey_or_script.to_string(),
        )?;
    }
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use dubp_client::wallet::prelude::SourceAmount;

    #[test]
    fn test_balance() -> anyhow::Result<()> {
        let mut client = MockGvaClient::default();
        client.expect_account_balance().returning(|_, _| {
            Ok(Some(AccountBalance {
                amount: SourceAmount::new(2046, 0),
                ud_amount_opt: None,
            }))
        });
        let mut out = Vec::new();
        balance(&client, &mut out, "toto", false)?;
        let output = std::str::from_utf8(&out)?;

        assert_eq!(output, "The balance of account 'SIG(toto)' is 20.46 Ğ1 !\n");

        Ok(())
    }

    #[test]
    fn test_balance_with_ud_unit() -> anyhow::Result<()> {
        let mut client = MockGvaClient::default();
        client.expect_account_balance().returning(|_, _| {
            Ok(Some(AccountBalance {
                amount: SourceAmount::new(2_046, 0),
                ud_amount_opt: Some(SourceAmount::new(1_023, 0)),
            }))
        });
        let mut out = Vec::new();
        balance(&client, &mut out, "toto", true)?;
        let output = std::str::from_utf8(&out)?;

        assert_eq!(
            output,
            "The balance of account 'SIG(toto)' is 2.00 UDĞ1 !\n"
        );

        Ok(())
    }
}
