//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;
use dubp_client::{
    crypto::{
        bases::b58::ToBase58,
        dewif::{
            read_dewif_file_content, write_dewif_v4_content, Currency, ExpectedCurrency,
            G1_CURRENCY,
        },
        keys::{
            ed25519::bip32::{KeyPair, PrivateDerivationPath},
            KeyPair as _, KeyPairEnum,
        },
        mnemonic::{mnemonic_to_seed, Language, Mnemonic, MnemonicType},
        utils::U31,
    },
    wallet::prelude::SourceAmount,
};

#[allow(clippy::large_enum_variant)]
#[derive(StructOpt)]
pub enum WalletCommand {
    /// Generate a wallet
    Gen {
        #[structopt(short, long, default_value = "en")]
        lang: Language,
        #[structopt(long, default_value = "12")]
        log_n: u8,
        #[structopt(short, long)]
        mnemonic_only: bool,
        #[structopt(long)]
        password_stdin: bool,
        /// Mnemonic word count
        #[structopt(short, long, parse(try_from_str = parse_mnemonic_len), default_value = "12")]
        word_count: MnemonicType,
    },
    /// Get a public key
    Pubkey {
        account_index: u32,
        address_index: Option<u32>,
        #[structopt(long)]
        dewif: Option<String>,
        #[structopt(long, parse(from_os_str))]
        dewif_file: Option<PathBuf>,
        #[structopt(short, long)]
        external: bool,
        #[structopt(short, long, default_value = "en")]
        lang: Language,
        #[structopt(long)]
        password_stdin: bool,
    },
    /// Import a wallet from mnemonic
    Import {
        #[structopt(short, long, default_value = "en")]
        lang: Language,
        #[structopt(long, default_value = "12")]
        log_n: u8,
        #[structopt(long)]
        password_stdin: bool,
    },
    /// send money
    Pay {
        account_index: u32,
        #[structopt(short, long)]
        amount: f64,
        #[structopt(short, long)]
        base: Option<u64>,
        #[structopt(short, long)]
        comment: Option<String>,
        #[structopt(long)]
        dewif: Option<String>,
        #[structopt(long, parse(from_os_str))]
        dewif_file: Option<PathBuf>,
        #[structopt(short, long, default_value = "en")]
        lang: Language,
        #[structopt(long)]
        password_stdin: bool,
        #[structopt(short, long)]
        recipient: PkOrScript,
        /// UD units
        #[structopt(short, long)]
        ud_units: bool,
        /// Auto confirm (for non-interactive use)
        #[structopt(short, long)]
        yes: bool,
    },
}

pub(crate) fn wallet<C: GvaClient, W: Write>(
    gva_client: &C,
    out: &mut W,
    command: WalletCommand,
) -> anyhow::Result<()> {
    match command {
        WalletCommand::Gen {
            lang,
            mnemonic_only,
            log_n,
            password_stdin,
            word_count,
        } => {
            let mnemonic = Mnemonic::new(word_count, lang)
                .map_err(|_| anyhow::Error::msg("unspecified rand error"))?;

            if mnemonic_only {
                writeln!(out, "{}", mnemonic.phrase())?;
            } else {
                writeln!(out, "Mnemonic: {}", mnemonic.phrase())?;
                gen_dewif(out, log_n, mnemonic, password_stdin)?;
            }

            Ok(())
        }
        WalletCommand::Import {
            lang,
            log_n,
            password_stdin,
        } => {
            let mnemonic_phrase = if let Some(mnemonic_env) = var_os("MNEMONIC") {
                mnemonic_env
                    .into_string()
                    .map_err(|_| anyhow::Error::msg("invalid utf8 string"))?
            } else {
                rpassword::prompt_password_stdout("Mnemonic: ")?
            };
            let mnemonic = Mnemonic::from_phrase(mnemonic_phrase, lang)?;
            gen_dewif(out, log_n, mnemonic, password_stdin)?;
            Ok(())
        }
        WalletCommand::Pubkey {
            account_index,
            address_index,
            dewif,
            dewif_file,
            external,
            lang,
            password_stdin,
        } => {
            let keypair = get_master_keypair(dewif, dewif_file, lang, password_stdin)?;

            let derivation_path = match account_index % 3 {
                0 => PrivateDerivationPath::transparent(U31::new(account_index)?)?,
                1 => todo!(),
                2 => PrivateDerivationPath::opaque(
                    U31::new(account_index)?,
                    external,
                    if let Some(address_index) = address_index {
                        Some(U31::new(address_index)?)
                    } else {
                        None
                    },
                )?,
                _ => unreachable!(),
            };

            let derived_keypair = keypair.derive(derivation_path);

            writeln!(out, "{}", derived_keypair.public_key().to_base58())?;

            Ok(())
        }
        WalletCommand::Pay {
            account_index,
            amount,
            base,
            comment,
            dewif,
            dewif_file,
            lang,
            password_stdin,
            recipient,
            ud_units,
            yes,
        } => {
            let recipient = recipient.0;
            let keypair = get_master_keypair(dewif, dewif_file, lang, password_stdin)?;

            let (amount, amount_str) = if ud_units {
                (Amount::Uds(amount), format!("{:.2} UDğ1", amount))
            } else {
                (
                    Amount::Cents(SourceAmount::new(
                        (amount * 100.0).round() as i64,
                        base.unwrap_or_default() as i64,
                    )),
                    format!("{:.2} Ğ1", amount),
                )
            };

            match account_index % 3 {
                0 => {
                    let trasparent_keypair = keypair.derive(PrivateDerivationPath::transparent(
                        U31::new(account_index)?,
                    )?);

                    let confirm = if yes {
                        true
                    } else {
                        let res = input()
                            .msg(format!(
                                "Send {} to {} from transparent account {}? [y/N]",
                                amount_str,
                                recipient.to_string(),
                                trasparent_keypair.public_key().to_base58()
                            ))
                            .default('N')
                            .get();
                        res == 'y' || res == 'Y'
                    };

                    if confirm {
                        let req_time = Instant::now();
                        let payment_result = gva_client.simple_payment(
                            amount,
                            &trasparent_keypair.generate_signator(),
                            recipient,
                            comment,
                            None,
                        )?;

                        if let PaymentResult::Errors(errors) = payment_result {
                            writeln!(out, "All or part of the payment failed, errors: \n")?;
                            for error in errors {
                                writeln!(out, "- {:?}", error)?;
                            }
                            todo!()
                        } else {
                            let duration = req_time.elapsed();
                            writeln!(
                                out,
                                "Payment succesfully processed in {}.{} ms.",
                                duration.as_millis(),
                                duration.subsec_micros() % 1_000
                            )?;
                        }
                    }

                    Ok(())
                }
                1 => todo!(),
                2 => todo!(),
                _ => unreachable!(),
            }
        }
    }
}

fn parse_mnemonic_len(s: &str) -> anyhow::Result<MnemonicType> {
    Ok(MnemonicType::for_word_count(s.parse()?)?)
}

fn gen_dewif<W: Write>(
    out: &mut W,
    log_n: u8,
    mnemonic: Mnemonic,
    password_stdin: bool,
) -> anyhow::Result<()> {
    let seed = mnemonic_to_seed(&mnemonic);
    let keypair = KeyPair::from_seed(seed.clone());

    let password = if password_stdin {
        rpassword::read_password()?
    } else {
        rpassword::prompt_password_stdout("Password: ")?
    };
    //println!("TMP DEBUG: password={}", password);
    let dewif = write_dewif_v4_content(
        Currency::from(G1_CURRENCY),
        log_n,
        &password,
        &keypair.public_key(),
        seed,
    );

    writeln!(out, "DEWIF: {}", dewif)?;

    Ok(())
}

fn get_master_keypair(
    dewif_opt: Option<String>,
    dewif_file_opt: Option<PathBuf>,
    lang: Language,
    password_stdin: bool,
) -> anyhow::Result<KeyPair> {
    if dewif_opt.is_some() || dewif_file_opt.is_some() {
        let dewif_content = get_dewif_content(dewif_opt, dewif_file_opt)?;
        get_master_keypair_from_dewif(dewif_content, password_stdin)
    } else {
        let mnemonic_phrase = rpassword::prompt_password_stdout("Mnemonic: ")?;
        let mnemonic = Mnemonic::from_phrase(mnemonic_phrase, lang)?;
        Ok(KeyPair::from_seed(mnemonic_to_seed(&mnemonic)))
    }
}

fn get_dewif_content(
    dewif_opt: Option<String>,
    dewif_file_opt: Option<PathBuf>,
) -> anyhow::Result<String> {
    if let Some(dewif_env) = var_os("DEWIF") {
        dewif_env
            .into_string()
            .map_err(|_| anyhow::Error::msg("invalid utf8 string"))
    } else if let Some(dewif_file_env) = var_os("DEWIF_FILE") {
        let mut buf_reader = BufReader::new(File::open(dewif_file_env)?);
        let mut contents = String::new();
        buf_reader.read_to_string(&mut contents)?;
        Ok(contents)
    } else if let Some(dewif) = dewif_opt {
        Ok(dewif)
    } else if let Some(dewif_file) = dewif_file_opt {
        let mut buf_reader = BufReader::new(File::open(dewif_file)?);
        let mut contents = String::new();
        buf_reader.read_to_string(&mut contents)?;
        Ok(contents)
    } else {
        Err(anyhow::Error::msg("no DEWIF provided"))
    }
}

fn get_master_keypair_from_dewif(
    dewif_content: String,
    password_stdin: bool,
) -> anyhow::Result<KeyPair> {
    let password = if password_stdin {
        rpassword::read_password()?
    } else {
        rpassword::prompt_password_stdout("Password: ")?
    };

    let mut keypairs = read_dewif_file_content(
        ExpectedCurrency::Specific(Currency::from(G1_CURRENCY)),
        &dewif_content,
        &password,
    )?;

    if let Some(KeyPairEnum::Bip32Ed25519(keypair)) = keypairs.next() {
        Ok(keypair)
    } else {
        Err(anyhow::Error::msg("DEWIF corrupted"))
    }
}
