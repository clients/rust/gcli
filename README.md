# GVA Rust Client

A simple command line client written in Rust that use Duniter GVA API.

## Use

### Generate new wallet

Use command `wallet gen`:

```bash
gcli wallet gen
Mnemonic: sibling pelican sense stereo plastic helmet book expand tube whale census multiply
Password: 
DEWIF: AAAABAAAAAEMGzFNV+j80uJkBuAfazfFWZfz0ARlA5T/Ea3ZQTo7zjTiALR/jkf/pcBLFlsqtERCLozLcRP8Q74qj1NSFZ79Fw==
```

You can generate a mnemonic only with option `--mnemonic-only`:

```bash
gcli wallet gen --mnemonic-only
sibling pelican sense stereo plastic helmet book expand tube whale census multiply
```

You can choose another language, change mnemonic length, etc. See `wallet gen --help` for details.

#### Non-Interactive

You can choose a password in a non-interactive way with option `--password-stdin`:

```bash
echo "1234" | gcli wallet gen --password-stdin
Mnemonic: sibling pelican sense stereo plastic helmet book expand tube whale census multiply
DEWIF: AAAABAAAAAEMGzFNV+j80uJkBuAfazfFWZfz0ARlA5T/Ea3ZQTo7zjTiALR/jkf/pcBLFlsqtERCLozLcRP8Q74qj1NSFZ79Fw==
```

### Import a wallet form mnemonic

```bash
gcli wallet import
Mnemonic: sibling pelican sense stereo plastic helmet book expand tube whale census multiply
Password: 
DEWIF: AAAABAAAAAEMGzFNV+j80uJkBuAfazfFWZfz0ARlA5T/Ea3ZQTo7zjTiALR/jkf/pcBLFlsqtERCLozLcRP8Q74qj1NSFZ79Fw==
```

You can choose another language, and other parameters. See `wallet import --help` for details.

#### Non-Interactive

You can choose a mnemonic and password in a non-interactive way with environment variable `MNEMONIC` and option `--password-stdin`:

```bash
echo "1234" | MNEMONIC="sibling pelican sense stereo plastic helmet book expand tube whale census multiply" gcli wallet import --password-stdin
DEWIF: AAAABAAAAAEMGzFNV+j80uJkBuAfazfFWZfz0ARlA5T/Ea3ZQTo7zjTiALR/jkf/pcBLFlsqtERCLozLcRP8Q74qj1NSFZ79Fw==
```

### Get a public key (=address)

To obtain the public key of the sub-account whose derivation number is `3`:

```bash
gcli wallet get-pubkey --dewif AAAABAAAAAEMGzFNV+j80uJkBuAfazfFWZfz0ARlA5T/Ea3ZQTo7zjTiALR/jkf/pcBLFlsqtERCLozLcRP8Q74qj1NSFZ79Fw== 3
Password: 
2S7wAUzaXfG3Z1SWNULqBotnUb9QbucmeuVkqVL4X8FB
```

To obtain the master external* public key of the sub-account whose derivation number is `3`:

```bash
gcli wallet get-pubkey --external --dewif AAAABAAAAAEMGzFNV+j80uJkBuAfazfFWZfz0ARlA5T/Ea3ZQTo7zjTiALR/jkf/pcBLFlsqtERCLozLcRP8Q74qj1NSFZ79Fw== 3
Password: 
9KV3gJFcsEH98FQhF3vPW4YG15VGRybHZuzAeD3oVicx
```

* The master external public key allows you to generate all payment receipt addresses by public key derivation. This is the key to be configured on your server to generate the payment addresses that will be provided to your customers.

You can also obtain an external or internal address in particular by its derivation index, see `wallet get-pubkey --help` for details.

#### Non-Interactive

You can enter your password in a non-interactive way with option `--password-stdin`:

```bash
echo "1234" | gcli wallet get-pubkey --dewif AAAABAAAAAEMGzFNV+j80uJkBuAfazfFWZfz0ARlA5T/Ea3ZQTo7zjTiALR/jkf/pcBLFlsqtERCLozLcRP8Q74qj1NSFZ79Fw== 3
2S7wAUzaXfG3Z1SWNULqBotnUb9QbucmeuVkqVL4X8FB
```

## Contribute

Contributions are welcome :)

If you have any questions about the code don't hesitate to ask @elois on the duniter forum: [https://forum.duniter.org](https://forum.duniter.org)

The GraphQL schema is automatically generated from the GVA source code, to update the schema, use the following command:

cargo update -p duniter-gva-gql
